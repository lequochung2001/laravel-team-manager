<?php

declare(strict_types=1);

namespace App\Enums;

use Illuminate\Validation\Rules\Enum as RulesEnum;

final class StatusCommon extends RulesEnum
{
    const SUCCESS = 1;
    const FAIL = 0;
}

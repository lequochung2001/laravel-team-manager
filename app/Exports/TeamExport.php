<?php

namespace App\Exports;

use App\Repositories\Teams\TeamRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;

class TeamExport implements FromView
{
    private $request;
    public function __construct(
        Request $request,
        private TeamRepository $teamRepository
    ) {
        $this->request = $request;
    }

    public function view(): View
    {
        $teams = $this->teamRepository->getTeams(
            $this->request->get('search'),
            -1
        );
        return view('excel.teams', compact(
            'teams',
        ));
    }
}

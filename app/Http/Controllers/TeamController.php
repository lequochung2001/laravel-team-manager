<?php

namespace App\Http\Controllers;

use App\Enums\StatusCommon;
use App\Exports\TeamExport;
use App\Http\Requests\TeamRequest;
use App\Repositories\Departments\DepartmentRepository;
use App\Repositories\Teams\TeamRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class TeamController extends Controller
{
    public function __construct(
        private TeamRepository $teamRepository,
        private DepartmentRepository $departmentRepository,
    ) {
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        // Get teams based on search query and limit
        $teams = $this->teamRepository->getTeams(
            $request->get('search'),
            $request->get('limit')
        );

        // Get departments based on search query and limit
        $departments = $this->departmentRepository->getDepartment(
            $request->get('search'),
            $request->get('limit') ? $request->get('limit') : -1
        );

        // Return view with teams and departments
        return view('teams.index', [
            'teams' => $teams,
            'departments' => $departments,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TeamRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TeamRequest $request)
    {
        try {
            $data = [
                'team_id' => $request->team_id,
                'team_name' => $request->team_name,
                'department_id' => $request->department_id,
            ];
            // Create team data
            $this->teamRepository->create($data);

            // Redirect back with success status
            return redirect()->back()->with('status', StatusCommon::SUCCESS);
        } catch (\Exception $exception) {
            // Log error and redirect back with fail status
            Log::error($exception->getMessage());
            return redirect()->back()->with('status', StatusCommon::FAIL);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TeamRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TeamRequest $request)
    {
        try {

            $data = [
                'team_name' => $request->team_name,
                'department_id' => $request->department_id,
            ];
            $team = $this->teamRepository->getByTeamId($request->team_id);
            if (!$team) {
                return redirect()->back()->with('status', StatusCommon::FAIL);
            }
            // Update team data
            $this->teamRepository->updateByObject($team, $data);

            // Redirect back with success status
            return redirect()->back()->with('status', StatusCommon::SUCCESS);
        } catch (\Exception $exception) {
            // Log error and redirect back with fail status
            Log::error($exception->getMessage());
            return redirect()->back()->with('status', StatusCommon::FAIL);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        try {

            $team = $this->teamRepository->getByTeamId($id);
            if (!$team) {
                return redirect()->back()->with('status', StatusCommon::FAIL);
            }
            // Delete team data
            $this->teamRepository->delete($team);

            // Redirect back with success status
            return redirect()->back()->with('status', StatusCommon::SUCCESS);
        } catch (\Exception $exception) {
            // Log error and redirect back with fail status
            Log::error($exception->getMessage());
            return redirect()->back()->with('status', StatusCommon::FAIL);
        }
    }

    public function excel(Request $request)
    {
        return Excel::download(new TeamExport($request, $this->teamRepository), 'teams.xlsx');
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_id' => 'required|string',
            'team_name'=> 'required|string',
            'department_id'=> 'required|string',
        ];
    }

    /**
     * @return array
     */
    public function errorCode(): array
    {
        return [];
    }
}

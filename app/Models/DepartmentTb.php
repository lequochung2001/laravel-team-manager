<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepartmentTb extends Model
{
    protected $table = 'department_tb';
    protected $primaryKey = 'department_id';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $fillable = [
        'department_id',
        'department_name',
        'descriptions',
    ];

    public function departmentTb()
    {
        return $this->hasMany(TeamTb::class, 'department_id', 'department_id');
    }
}

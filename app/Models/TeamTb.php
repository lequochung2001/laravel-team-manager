<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamTb extends Model
{
    protected $table = 'team_tb';
    protected $primaryKey = 'team_id';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $fillable = [
        'team_id',
        'team_name',
        'department_id'
    ];

    public function departmentTb()
    {
        return $this->belongsTo(DepartmentTb::class, 'department_id', 'department_id');
    }
}

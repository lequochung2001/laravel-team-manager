<?php

namespace App\Repositories;

abstract class BaseRepository
{
    protected $model;


    public function __construct()
    {
        $this->setModel();
    }


    abstract public function getModel();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make(
            $this->getModel()
        );
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        $result = $this->model->find($id);

        return $result;
    }

    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }

    public function update($id, $attributes = [])
    {
        $result = $this->find($id);
        if ($result) {
            $result->update($attributes);
            return $result;
        }

        return false;
    }

    public function delete($model)
    {
        $model->delete();
        return true;
    }

    public function whereId($model, $id)
    {
        return $model->where('id', $id);
    }
}

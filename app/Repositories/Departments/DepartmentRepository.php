<?php

namespace App\Repositories\Departments;

use App\Models\DepartmentTb;
use App\Repositories\BaseRepository;

class DepartmentRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function getModel(): string
    {
        return DepartmentTb::class;
    }

    /**
     * Retrieve a paginated list of Team models filtered by search criteria.
     *
     * @param string|null $search The search string to filter the teams.
     * @param int|null    $limit  The maximum number of teams to retrieve per page.
     * @return \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     *                          A paginated list of Team models if $limit is greater than 0,
     *                          otherwise a collection of all Team models.
     */
    public function getDepartment(
        string|null $search = '',
        int|null $limit = 10
    ) {
        $departmentTb = new $this->model;

        return $limit > 0 ? $departmentTb->paginate($limit) : $departmentTb->get();
    }

    /**
     * Create a new Department model instance with the given attributes.
     *
     * @param array $attributes The attributes to assign to the new Department instance.
     *                          This array should contain 'department_id', 'department_name',
     *                          and 'descriptions' keys corresponding to the attributes
     *                          of the Department model.
     * @return Department       The newly created Department model instance with the provided attributes.
     */
    public function create($attributes = [])
    {
        $department = new $this->model;

        $department->department_id = $attributes['department_id'];
        $department->department_name = $attributes['department_name'];
        $department->descriptions = $attributes['descriptions'];
        $department->save();

        return $department;
    }
}

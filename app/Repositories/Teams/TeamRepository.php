<?php

namespace App\Repositories\Teams;

use App\Models\TeamTb;
use App\Repositories\BaseRepository;

class TeamRepository extends BaseRepository
{
    const PAGINATE = 7;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function getModel(): string
    {
        return TeamTb::class;
    }

    /**
     * Retrieve a paginated list of Team models filtered by search criteria.
     *
     * @param string|null $search The search string to filter the teams.
     * @param int|null    $limit  The maximum number of teams to retrieve per page.
     * @return \Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     *                          A paginated list of Team models if $limit is greater than 0,
     *                          otherwise a collection of all Team models.
     */
    public function getTeams(
        string|null $search = '',
        int|null $limit = 0,
    ) {
        $teams = new $this->model;
        $teams = $this->searchString($teams, $search);
        $teams = $this->withDepartmentTb($teams);
        $limit = $limit ? $limit : self::PAGINATE;
        return $limit >= 0 ? $teams->paginate($limit) : $teams->get();
    }

    /**
     * Apply search criteria to the given model.
     *
     * @param TeamTb $model The model instance to apply search criteria to.
     * @param int|string|null $search The search string to filter the model.
     * @return TeamTb The modified model instance with search criteria applied.
     */
    public function searchString($model, int|string|null $search)
    {
        return $model->where(function ($query) use ($search) {
            $searchString = '%' . $search . '%';
            $query->where('team_name', 'like', $searchString);
            $query->orWhere('team_id', 'like', $searchString);
        });
    }


    /**
     * Create a new Team model instance with the given attributes.
     *
     * @param array $attributes The attributes to assign to the new Team instance.
     *                          This array should contain 'team_id', 'team_name', and
     *                          'department_id' keys corresponding to the attributes
     *                          of the Team model.
     * @return Team             The newly created Team model instance with the provided attributes.
     */
    public function create($attributes = [])
    {
        $team = new $this->model;

        $team->team_id = $attributes['team_id'];
        $team->team_name = $attributes['team_name'];
        $team->department_id = $attributes['department_id'];
        $team->save();

        return $team;
    }

    /**
     * Eager load the 'departmentTb' relationship for the given model.
     *
     * @param mixed $model The model instance to eager load the relationship for.
     * @return mixed      The model instance with the 'departmentTb' relationship loaded.
     */
    public function withDepartmentTb($model)
    {
        return $model->with('departmentTb');
    }

    /**
     * Retrieve a Team model instance by its team ID.
     *
     * @param string $teamId The ID of the team to retrieve.
     * @return mixed        The Team model instance matching the given team ID.
     */
    public function getByTeamId($teamId)
    {
        $team = new $this->model;
        $team = $this->whereTeamId($team, $teamId);
        return $team->first();
    }

    /**
     * Apply a where clause to the given model to filter by team ID.
     *
     * @param mixed $model  The model instance to apply the where clause to.
     * @param string $teamId The ID of the team to filter by.
     * @return mixed        The model instance with the where clause applied.
     */
    public function whereTeamId($model, string $teamId)
    {
        return $model->where('team_id', $teamId);
    }

    /**
     * Update the attributes of a Team model instance.
     *
     * @param Team $team      The Team model instance to update.
     * @param array $attributes The attributes to assign to the new Team instance.
     *                          This array should contain 'team_name' and
     *                          'department_id' keys corresponding to the attributes
     * @return Team           The updated Team model instance.
     */
    public function updateByObject($team, $attributes = [])
    {
        $team->team_name = $attributes['team_name'];
        $team->department_id = $attributes['department_id'];
        $team->save();

        return $team;
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('team_tb', function (Blueprint $table) {
            $table->string('team_id', 20)->primary();
            $table->string('team_name', 50);
            $table->string('department_id', 20);
            $table->foreign('department_id')->references('department_id')->on('department_tb');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('team_tb');
    }
};

<?php

namespace Database\Seeders;

use App\Repositories\Departments\DepartmentRepository;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    public function __construct(private DepartmentRepository $departmentRepository)
    {
    }

    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $data = [];
        $data[] = [
            'department_id' => "ADMIN",
            'department_name' => "Administration",
            'descriptions' => null,
        ];
        $data[] = [
            'department_id' => "DIRECTOR",
            'department_name' => "Director",
            'descriptions' => null,
        ];
        $data[] = [
            'department_id' => "DEVELOP",
            'department_name' => "Development",
            'descriptions' => null,
        ];
        foreach ($data as $array) {
            $this->departmentRepository->create($array);
        }
    }
}

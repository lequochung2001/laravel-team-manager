<?php

namespace Database\Seeders;

use App\Models\TeamTb;
use App\Repositories\Teams\TeamRepository;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class TeamSeeder extends Seeder
{
    public function __construct(private TeamRepository $teamRepository)
    {
    }

    /**
     * Seed the application's database.
     */
    public function run(Faker $faker): void
    {
        for ($i = 1; $i <= 100; $i++) {
            $string = $faker->company;
            $team_id = strtoupper(substr($string, 0, 2));

            $this->teamRepository->create([
                'team_id' => $team_id,
                'team_name' => $string,
                'department_id' => "DEVELOP"
            ]);
        }
    }
}

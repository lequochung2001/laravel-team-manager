<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body onload="window.print();">
    <h2 class="title">Team manager</h2>
    <div>
        <table id="order_table" class="table table-bordered dt-responsive  table-striped" style="width:100%">
            <thead>
                <tr>
                    <th style="width: 200px">Mã Team</th>
                    <th style="width: 100px">Tên Team</th>
                    <th style="width: 100px">Tên bộ phận</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($teams as $team)
                    <tr class="row-team">
                        <th class="team-id" scope="row">{{ $team->team_id }}</th>
                        <td class="team-name">{{ $team->team_name }}</td>
                        <td value="{{ $team->departmentTb->department_id }}" class="department-name">
                            {{ $team->departmentTb->department_name }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>

</html>

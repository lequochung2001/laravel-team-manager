<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Danh Sách Team</title>
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v6.5.1/css/all.css">
</head>

<body>

    <div class="row mt-3">
        <h4 class="text-center">Danh Sách Team</h4>
        <div class="col-md-12 row">
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <form action="{{ route('teams.index') }}" method="get">
                    @csrf
                    <div class=" input-group form-search">
                        <input id="input_search" type="text" placeholder="Enter a search term" class="form-control"
                            name="search">
                        <button class="btn bg-primary waves-effect waves-light input-group-text">
                            <i class="fas fa-search text-white"> </i>
                        </button>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
                <a href="{{ route('teams.excel') . '?search=' . request('search') }}" class="btn btn-success">Excel</a>
            </div>
        </div>
        <div class="col-md-12 mt-2 row">
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Mã Team</th>
                            <th scope="col">Tên Team</th>
                            <th scope="col">Tên bộ phận</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($teams as $team)
                            <tr class="row-team">
                                <th class="team-id" scope="row">{{ $team->team_id }}</th>
                                <td class="team-name">{{ $team->team_name }}</td>
                                <td value="{{ $team->departmentTb->department_id }}" class="department-name">
                                    {{ $team->departmentTb->department_name }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $teams->links() }}
            </div>
            <div class="col-md-5 ms-1">
                <form action="{{ route('teams.store') }}" method="POST" id="form-team">
                    @csrf
                    <div class="form-group row mb-3">
                        <label for="team_id" class="col-sm-2 col-form-label">Mã Team</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="team_id" name="team_id"
                                placeholder="Mã Team">
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="team_name" class="col-sm-2 col-form-label">Tên Team</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="team_name" name="team_name"
                                placeholder="Tên Team">
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="department_id" class="col-sm-2 col-form-label">Bộ Phận</label>
                        <div class="col-sm-10">
                            <select class="form-select" id="department_id" name="department_id"
                                aria-label="Default select example">
                                <option class="option-first" selected>Chọn Bộ Phận</option>
                                @foreach ($departments as $department)
                                    <option value="{{ $department->department_id }}">{{ $department->department_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row div-setting">
                        <button type="button" class="btn btn-primary col-md-2 add-team">Thêm</button>
                        <button type="button" class="btn btn-warning col-md-2 ms-2 update-team">Sữa</button>
                        <a onclick="return confirm('bạn có muốn thực hiện hành động này không?');"
                            class="btn btn-danger col-md-2 ms-2 delete-team">Xóa</a>
                    </div>
                </form>

            </div>
            <div class="col-md-1"></div>
        </div>

    </div>
    <script>
        localStorage.setItem('div_setting', $('.div-setting').html());
        //button add-team
        $(document).on('click', '.add-team', function() {
            // add route to action
            $("#form-team").attr('action', '{{ route('teams.store') }}');
            //change value input
            $("#team_id").attr('readonly', false);
            $("#team_id").val('');
            $("#team_name").val('');
            $("#department_id").val($("#department_id option:first").val());
            // change dom
            $('.div-setting').html(
                `
                <button type="submit" class="btn btn-primary col-md-2">Xác Nhận</button>
                <button type="button" class="btn btn-danger col-md-2 ms-2 cancel">Hủy</button>
            `
            );
            //remove class
            $('table').find('.bg-primary').removeClass('bg-primary text-white')
        })

        // click button cancel
        $(document).on('click', '.cancel', function() {
            $('.div-setting').html(localStorage.getItem('div_setting'));
        })

        // click tr row team
        $(document).on('click', '.row-team', function() {
            //remove class
            $('table').find('.bg-primary').removeClass('bg-primary text-white')
            //add class
            $(this).addClass('bg-primary text-white')
            //add url delete team to button
            url_delete_team = '{{ route('teams.delete', ':id') }}';
            url_delete_team = url_delete_team.replace(':id', $(this).find('.team-id').text())
            $('.delete-team').attr('href', url_delete_team)
        })

        //button click update-team
        $(document).on('click', '.update-team', function() {
            // add route to action
            $("#form-team").attr('action', '{{ route('teams.update') }}');
            tr_row_team = $('table').find('.bg-primary');
            //change value input
            $("#team_id").val(tr_row_team.find('.team-id').text());
            $("#team_name").val(tr_row_team.find('.team-name').text());
            $("#department_id").val(tr_row_team.find('.department-name').attr('value'));
            $("#team_id").attr('readonly', true);
            // change dom
            $('.div-setting').html(
                `
                <button type="submit" class="btn btn-primary col-md-2">Cập Nhật</button>
                <button type="button" class="btn btn-danger col-md-2 ms-2 cancel">Hủy</button>
            `
            );
        })
    </script>
</body>

</html>
